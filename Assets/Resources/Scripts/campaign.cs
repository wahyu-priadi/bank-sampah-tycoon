﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class campaign : MonoBehaviour {

	//Script Interaction
	public profile Profile;
	public economics Economic;
	public craft Craft;

	public GameObject action_button;
	public Button pamfletBtn, buletinBtn, majalahBtn, televisiBtn;
	public Button campaignPanelBtn, craftPanelBtn;
	public Toggle[] status; 


	public void getCampaignButton (Button campaignButton) {
		string cpgBtn = campaignButton.name;
		switch (cpgBtn) {
		case ("PamfletBtn"):
			Economic.SubMoneyForCampaign ("Pamflet");
			craft.getCampaignType ("Pamflet");
			craft.pamfletActive = true;
			break;
		case ("BuletinBtn"):
			Economic.SubMoneyForCampaign ("Buletin");
			Economic.getCampaignType ("Buletin");
			economics.buletinActive = true;
			break;
		case ("MajalahBtn"):
			Economic.SubMoneyForCampaign ("Majalah");
			craft.getCampaignType("Majalah");
			craft.majalahActive = true;
			break;
		case ("TelevisiBtn"):
			Economic.SubMoneyForCampaign ("Televisi");
			Economic.getCampaignType ("Televisi");
			economics.televisiActive = true;
			break;
		} //Pilih media sosialisasi
	}

	public void closeCampaignPanelBtn(){
		this.gameObject.SetActive (false);
		action_button.SetActive (true);
		Time.timeScale = 1;
		Profile.timeContinue = true;
	}

	void Update(){
		#region Cek Status Media
		//Cek Status Pamflet
		if (Economic.money < 100000f && !craft.pamfletActive) {
			pamfletBtn.interactable = false;
			status [0].isOn = false;
		} else if (craft.pamfletActive) {
			pamfletBtn.interactable = false;
			status [0].isOn = true;
		} else {
			pamfletBtn.interactable = true;
		}

		//Cek Status Buletin
		if (Economic.money < 200000f && !economics.buletinActive) {
			buletinBtn.interactable = false;
			status [1].isOn = false;
		} else if (economics.buletinActive) {
			buletinBtn.interactable = false;
			status [1].isOn = true;
		} else
			buletinBtn.interactable = true;

		//Cek Status Majalah
		if (Economic.money < 300000f && !craft.majalahActive) {
			majalahBtn.interactable = false;
			status [2].isOn = false;
		} else if (craft.majalahActive) {
			majalahBtn.interactable = false;
			status [2].isOn = true;
		} else
			majalahBtn.interactable = true;


		if (Economic.money < 400000f && !economics.televisiActive) {
			televisiBtn.interactable = false;
			status [3].isOn = false;
		} else if (economics.televisiActive) {
			televisiBtn.interactable = false;
			status [3].isOn = true;
		} else 
			televisiBtn.interactable = true;
		#endregion
	}
}
