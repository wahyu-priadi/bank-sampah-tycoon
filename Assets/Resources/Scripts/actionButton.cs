﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class actionButton : MonoBehaviour {

	//Script Interaction
	public profile Profile;
	public backgroundMusic BackgroundMusic;
	public soundEffect SoundEffect;
	//public information Information;

	public bool enCraftPanel = false;
	public bool sfxON, bgmON;
	public GameObject meja_craft, crew_meja, action_button;
	private GameObject[] allObject1, allObject2;
	public GameObject campaign_panel, craft_panel, settingPanel, dimPanel, confirmSave, confirmExit;
	public Toggle bgmCheck, sfxCheck;
	public Transform[] posisi_crew;
	public Button craftPanelBtn, campaignPanelBtn;
	public GameObject[] assigned_meja, assigned_crew, smoke_work, unlockCrew;

	public int checkLevel;

	void Start () {

		bgmON = sfxON = true;

		craftPanelBtn.interactable = true;
		campaignPanelBtn.interactable = true;

		meja_craft = Resources.Load<GameObject>("Prefabs/meja_craft") as GameObject;
		crew_meja = Resources.Load<GameObject> ("Prefabs/crew_play") as GameObject;

		for (int c = 0; c < 6; c++) {
			assigned_meja [c] = Instantiate (meja_craft, posisi_crew [c].transform.position, posisi_crew [c].transform.rotation) as GameObject;
			assigned_crew [c] = Instantiate (crew_meja, posisi_crew [c].transform.position, posisi_crew [c].transform.rotation) as GameObject;
			assigned_crew [c].SetActive (false);
		} //Instatiate meja dan crew
	}	

	void Update () {
		if (Profile.timeContinue == true) {
			if (profile.level < 3) {
				checkLevel = 1;
			} else if (profile.level < 5) {
				checkLevel = 3;
			} else if (profile.level < 7) {
				checkLevel = 5;
			} else if (profile.level < 9) {
				checkLevel = 7;
			} else if (profile.level < 11) {
				checkLevel = 9;
			} else if (profile.level >= 11) {
				checkLevel = 11;
			} 

			switch (checkLevel) {
			case 1:
				assigned_meja [4].SetActive (false);
				assigned_crew [4].SetActive (true);
				break;
			case 3:
				assigned_meja [2].SetActive (false);
				assigned_crew [2].SetActive (true);
				unlockCrew [2].SetActive (false);
				goto case 1;
			case 5:
				assigned_meja [0].SetActive (false);
				assigned_crew [0].SetActive (true);
				unlockCrew [0].SetActive (false);
				goto case 3;
			case 7:
				assigned_meja [5].SetActive (false);
				assigned_crew [5].SetActive (true);
				unlockCrew [5].SetActive (false);
				goto case 5;
			case 9:
				assigned_meja [3].SetActive (false);
				assigned_crew [3].SetActive (true);
				unlockCrew [3].SetActive (false);
				goto case 7;
			case 11:
				assigned_meja [1].SetActive (false);
				assigned_crew [1].SetActive (true);
				unlockCrew [1].SetActive (false);
				goto case 9;
			}


		}
	}

	public void OnClickCraftButton(){
		if (craftPanelBtn.interactable == true) {
			Profile.timeContinue = false;
			Time.timeScale = 0;

			craft_panel.SetActive (true);
			action_button.SetActive (false);
			Profile.timeContinue = false;
		}
	}  //Craft Button

	public void OnClickCampaignButton(){
		if (campaignPanelBtn.interactable == true) {
			Profile.timeContinue = false;
			Time.timeScale = 0;

			campaign_panel.SetActive (true);
			action_button.SetActive (false);
			Profile.timeContinue = false;
		}
	} //Campaign Button

	public void OpenMenu(){ 
		settingPanel.SetActive (true);
		dimPanel.SetActive (true);
		Time.timeScale = 0;

		action_button.SetActive (false);
		Profile.timeContinue = false;
	} //Open Menu
		
	public void ToSave(){
		confirmSave.SetActive (true);
	}

	#region Exit to Main Menu
	public void ExitToMainMenu(){
		confirmExit.SetActive (true);
	}

	public void ConfirmExit(){
		confirmExit.SetActive (false);
		settingPanel.SetActive (false);
		dimPanel.SetActive (false);
		allObject1 = GameObject.FindGameObjectsWithTag ("Bank Sampah");
		allObject2 = GameObject.FindGameObjectsWithTag ("Crew");
		foreach (GameObject o in allObject1) {
			Destroy (o.gameObject);
		}
		foreach (GameObject c in allObject2) {
			Destroy (c.gameObject);
		}
		SceneManager.LoadSceneAsync("scene0", LoadSceneMode.Single);

	}

	public void CancelExit(){
		if (confirmExit.activeSelf) {
			confirmExit.SetActive (false);
		} else if (confirmSave.activeSelf) {
			confirmSave.SetActive (false);
		}
	}
	#endregion //Exit to Main Menu

	public void bgmToggle (bool bgm){
		if (!bgm) {
			BackgroundMusic.bgMusic.mute = true;
			bgmON = false;
			bgmCheck.isOn = false;
		} else if (bgm) {
			BackgroundMusic.bgMusic.mute = false;
			bgmON = true;
			bgmCheck.isOn = true;
		}
	}

	public void sfxToggle (bool sfx){
		if (!sfx) {
			foreach (AudioSource aud in SoundEffect.audioEvent){
				aud.mute = true;
			}
			SoundEffect.regClick.mute = true;
			sfxON = false;
			sfxCheck.isOn = false;
		} else if (sfx){
			foreach (AudioSource aud in SoundEffect.audioEvent){
				aud.mute = false;
			}
			SoundEffect.regClick.mute = false;
			sfxON = true;
			sfxCheck.isOn = true;
		}
	}

	public void ExitMenu(){
		settingPanel.SetActive (false);
		dimPanel.SetActive (false);
		Time.timeScale = 1;
		action_button.SetActive (true);
		Profile.timeContinue = true;
	} //Exit Menu
}


