﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class tutorial : MonoBehaviour {

	public profile Profile;

	public string tutorialText;
	public bool tutorialOn;
	public GameObject[] tutorialPanel;
	int number;

	void Start(){
		if (!menu.loadIsTrue) {
			tutorialPanel [number].SetActive (true);
			Time.timeScale = 0;
			Profile.timeContinue = false;
			craft.pamfletActive = false;
			economics.buletinActive = false;
			craft.majalahActive = false;
			economics.televisiActive = false;

		}
	}

	public void NextTutorial(){
		number++;
		Debug.Log (number);
		if (number != 13) {
			tutorialPanel [number - 1].SetActive (false);
			tutorialPanel [number].SetActive (true);
		} else if (number == 13){
			tutorialPanel [number - 1].SetActive (false);
			this.gameObject.SetActive (false);
			number = 0;
			Time.timeScale = 1;
			Profile.timeContinue = true;
		}
	}
		
	void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			tutorialPanel [number].SetActive (false);
			Time.timeScale = 1;
			Profile.timeContinue = true;
			number = 0;
			this.gameObject.SetActive (false);
		}
	}

}
