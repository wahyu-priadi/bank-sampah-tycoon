﻿using UnityEngine;
using System.Collections;


public class rank : MonoBehaviour {

	//Script Interaction
	public profile Profile;
	public economics Economic;
	public information Information;
	public actionButton ActionButton;

	public float playerScore, comScore;
	public float[] rankList;
	public float currentRank;

	void Start (){
		if (!menu.loadIsTrue) {
			currentRank = 0;
		} 
	}

	public void doRank(){
		playerScore = profile.exp;
		Debug.Log (playerScore);
		comScore = 50;

		float[] rankList = new float[21];
		rankList [0] = playerScore;

		for (int x = 1; x < 21; x++) {
			rankList [x] = comScore;
			float rrg = Random.Range (300, 500);
			comScore = comScore + rrg;
		}
			
		float[] sortedRank = Bubblesort (rankList);
		float rank = 21f - System.Array.IndexOf (sortedRank, playerScore);
		currentRank = rank;
		Debug.Log (currentRank);
//		foreach (var item in sortedRank) {
//			Debug.Log (item.ToString ());
//		}
		Information.Ranked (currentRank);
	}

	private static float[] Bubblesort (float[] rankList){
		
		int length = rankList.Length;
		for (int i = 0; i < length - 1; i++) {
			for (int j = 0; j < length - 1 - i; j++) {
				if (rankList [j] > rankList [j + 1]) {
					float tmp = rankList [j];
					rankList [j] = rankList [j + 1];
					rankList [j + 1] = tmp;
				}
			}
		}

		return rankList;
	}
		
}
