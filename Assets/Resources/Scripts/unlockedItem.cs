﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class unlockedItem : MonoBehaviour {

	public GameObject newCraftItem, newCampaignItem, newCrewAdded;

	void Start () {
		
	}
	
	public void NewCraftingItems(){
		newCraftItem.SetActive (true);
	}

	public void NewCampaignItems(){
		newCampaignItem.SetActive (true);
	}

	public void NewCrewAdded(){
		newCrewAdded.SetActive (true);
	}
}
