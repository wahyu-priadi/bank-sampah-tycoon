﻿using UnityEngine;
using System.Collections;

public class pathPeople : MonoBehaviour {

	public Transform currentPath;
	public Transform[] Path;
	public NavMeshAgent queuePeople;
	private int y = 0;


	void Start () {

		queuePeople.destination = Path [y].position;
	}
		
	// Update is called once per frame
	void Update () {
		var distPath = Vector3.Distance (Path [y].position, transform.position);
		currentPath = Path [y];
		if (y != 4) {
			if (distPath < 0.5) {
				if (y < Path.Length - 1) {
					y++;
					queuePeople.destination = Path [y].position;
				} 
			}
		} else {
			Destroy (this.gameObject);
		}
	}

}
