﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class craft : MonoBehaviour {

	//Script Interaction
	public GameObject progressBar;
	public information Information;
	public economics Eco;
	public profile Profile;
	public craftProgress CraftProgress;

	public int[] craftable; 
	public GameObject gridCraft, amountCraft, craftPanel, action_button;
	public Button craftPanelBtn, campaignPanelBtn;
	public Image craftImage;
	public Text craftTitle, fabricText, paperText, plasticText, earnMoneyExp, amountText, durationText;
	public string craftingName, craftBtn, changeBtn;
	public float durationReqPra, crewEffect;
	public float earnMoneyDft, earnMoneyReq, earnMoneyPra;
	public float earnExpDft, earnExpReq;

	public float durationDft, durationReq;
	public static float campaignEffect;
	public static bool pamfletActive, majalahActive = false;


	private int fabricReq, paperReq, plasticReq, 
	fabricDft, paperDft, plasticDft,
	fabricCfm, paperCfm, plasticCfm;

	public static int multi;

	private int amount;
	private int sumcraftable, craftLevel, crewLevel;

	void Start () {
		craftable = new int[3];

		fabricCfm = Eco.fabricRes;
		paperCfm = Eco.paperRes;
		plasticCfm = Eco.plasticRes;
		multi = 1;
		crewEffect = 0;

		if (pamfletActive) {
			getCampaignType ("Pamflet");
		} else if (majalahActive) {
			getCampaignType ("Majalah");
		}

	}

	public static void getCampaignType(string type){
		switch (type) {
		case ("Pamflet"):
			campaignEffect = 0.2f;
			break;
		case ("Majalah"):
			campaignEffect = 0.5f;
			break;
		}
	} //Cek Campaign Type

	public void getCraftDesc (Button craftButton){
		multi = 1;
		amountText.text = multi.ToString();
		craftBtn = craftButton.name;
		#region choose crafting
		switch (craftBtn) {
		case ("potTanamanBtn"): //level 1 (bergambar)
			craftingName = "Pot Tanaman";
			fabricDft = fabricReq = 1;
			paperDft = paperReq = 1;
			plasticDft = plasticReq = 1;

			durationDft = durationReq = 2;
			earnMoneyDft = earnMoneyReq = 2000;
			earnExpDft = earnExpReq = 50;
			break;
		case ("celenganBtn"): //level 2 (bergambar)
			craftingName = "Celengan";
			fabricDft = fabricReq = 3;
			paperDft = paperReq = 0;
			plasticDft = plasticReq = 1;

			durationDft = durationReq = 3;
			earnMoneyDft = earnMoneyReq = 7000;
			earnExpDft = earnExpReq = 70;
			break;
		case ("tmpTisuBtn"): //level 3 (bergambar)
			craftingName = "Tempat Tisu";
			fabricDft = fabricReq = 1;
			paperDft = paperReq = 5;
			plasticDft = plasticReq = 0;

			durationDft = durationReq = 4;
			earnMoneyDft = earnMoneyReq = 15000;
			earnExpDft = earnExpReq = 90;
			break;
		case ("tasBelanjaBtn"): //level 4
			craftingName = "Tas Belanja";
			fabricDft = fabricReq = 2;
			paperDft = paperReq = 0;
			plasticDft = plasticReq = 8;

			durationDft = durationReq = 8;
			earnMoneyDft = earnMoneyReq = 15000;
			earnExpDft = earnExpReq = 150;
			break;
		case ("tmpPensilBtn"): //level 5
			craftingName = "Tempat Pensil";
			fabricDft = fabricReq = 2;
			paperDft = paperReq = 6;
			plasticDft = plasticReq = 2;

			durationDft = durationReq = 7;
			earnMoneyDft = earnMoneyReq = 10000;
			earnExpDft = earnExpReq = 120;
			break;
		case ("mobilMiniaturBtn"): //level 10
			craftingName = "Miniatur Mobil";
			fabricDft = fabricReq = 0;
			paperDft = paperReq = 13;
			plasticDft = plasticReq = 4;

			durationDft = durationReq = 9;
			earnMoneyDft = earnMoneyReq = 20000;
			earnExpDft = earnExpReq = 320;
			break;
		case ("kapalMiniaturBtn"): //level 10
			craftingName = "Miniatur Kapal"; 
			fabricDft = fabricReq = 0;
			paperDft = paperReq = 14;
			plasticDft = plasticReq = 3;

			durationDft = durationReq = 9;
			earnMoneyDft = earnMoneyReq = 25000;
			earnExpDft = earnExpReq = 320;
			break;
	
		case ("celemekBtn"): //level 12
			craftingName = "Celemek";
			fabricDft = fabricReq = 5;
			paperDft = paperReq = 0;
			plasticDft = plasticReq = 12;

			durationDft = durationReq = 11;
			earnMoneyDft = earnMoneyReq = 35000;
			earnExpDft = earnExpReq = 550;
			break;
		case ("bungaHiasBtn"): //level 15
			craftingName = "Bunga Hias";
			fabricDft = fabricReq = 10;
			paperDft = paperReq = 5;
			plasticDft = plasticReq = 5;

			durationDft = durationReq = 12;
			earnMoneyDft = earnMoneyReq = 45000;
			earnExpDft = earnExpReq = 850;
			break;
		case ("tikarBtn"): //level 14 
			craftingName = "Tikar";
			fabricDft = fabricReq = 10;
			paperDft = paperReq = 0;
			plasticDft = plasticReq = 15;

			durationDft = durationReq = 14;
			earnMoneyDft = earnMoneyReq = 75000;
			earnExpDft = earnExpReq = 1050;
			break;
		}
		fabricReq = fabricDft * multi;
		paperReq = paperDft * multi;
		plasticReq = plasticDft * multi;
		earnMoneyPra = earnMoneyDft * multi;
		earnMoneyReq = earnMoneyPra + (earnMoneyPra * campaignEffect);
		#endregion
		amountCraft.SetActive (true);
	} //Memilih kerajinan yang akan dibuat

	public void changeAmount(Button change){
		changeBtn = change.name;
		switch (changeBtn) {
		case ("Decrease"):
			if (multi != 1) {
				multi--;
			} else if (multi == 1) {
				multi = 1;
			}
			goto case ("Hitung");
		case ("Increase"):
			multi++;
			goto case ("Hitung");
		case ("Hitung"):
			fabricReq = fabricDft * multi;
			paperReq = paperDft * multi;
			plasticReq = plasticDft * multi;

			//durationReqPra = durationDft * multi;
			//durationReq = durationReqPra - (durationReqPra * crewEffect);

			earnMoneyPra = earnMoneyDft * multi;
			earnMoneyReq = earnMoneyPra + (earnMoneyPra * campaignEffect);

			earnExpReq = earnExpDft * multi;
			amountText.text = multi.ToString ();
			break;
		}
	} //Menambah atau mengurangi banyaknya kerajinan yang dibuat

	public void backConfirmClose (Button bcc){
		switch (bcc.name) {
		case ("BackBtn"):
			gridCraft.SetActive (true);
			amountCraft.SetActive (false);
			break;
		case ("ConfirmBtn"):
			durationReq = durationDft - (durationDft * crewEffect);
			if (sumcraftable != 3) {
				Debug.Log ("Tidak Bisa Dibuat");
			} else {
				
				fabricCfm = Eco.fabricRes - fabricReq;
				paperCfm = Eco.paperRes - paperReq;
				plasticCfm = Eco.plasticRes - plasticReq;

				Eco.fabricRes = fabricCfm;
				Eco.paperRes = paperCfm;
				Eco.plasticRes = plasticCfm;


				progressBar.SetActive (true);
				CraftProgress.doProgressRun = true;

				gridCraft.SetActive (true);
				amountCraft.SetActive (false);
				craftPanel.SetActive (false);
				craftPanelBtn.interactable = false;
				campaignPanelBtn.interactable = false;
				Time.timeScale = 1;
				Profile.timeContinue = true;
			}
			break;
		case ("closeCraftPanel"):
			craftPanel.SetActive (false);
			action_button.SetActive (true);
			Time.timeScale = 1;
			Profile.timeContinue = true;
			break;
		}
	} //Tombol Kembali, Buat, Tutup Panel
		
	void Update () {
		#region craftable
		if (fabricReq > Eco.fabricRes) {
			fabricText.color = Color.red;
			craftable[0] = 0;
		} else {
			fabricText.color = Color.black;
			craftable[0] = 1;
		}

		if (paperReq > Eco.paperRes) {
			paperText.color = Color.red;
			craftable[1] = 0;
		} else {
			paperText.color = Color.black;
			craftable[1] = 1;
		}

		if (plasticReq > Eco.plasticRes) {
			plasticText.color = Color.red;
			craftable[2] = 0;
		} else {
			plasticText.color = Color.black;
			craftable[2] = 1;
		}
		sumcraftable = craftable [0] + craftable [1] + craftable [2];
		#endregion //Penentu kerajianan dapat dibuat atau tidak

		#region levelcheck
		if (profile.level < 2){
			craftLevel = 1;
		} else if (profile.level < 3){
			craftLevel = 2;
		} else if (profile.level < 4){
			craftLevel = 3;
				crewLevel = 3;
		} else if (profile.level < 5){
			craftLevel = 4;
		} else if (profile.level < 6){
			craftLevel = 5;
				crewLevel = 5;
		} else if (profile.level < 8){
			craftLevel = 7;
		} else if (profile.level < 10){
			craftLevel = 9;
				crewLevel = 9;
		} else if (profile.level < 12){
			craftLevel = 11;
				crewLevel = 10;
		} else if (profile.level < 13){
			craftLevel = 12;
				crewLevel = 13;
		} else if (profile.level >= 12){
			craftLevel = 12;
				crewLevel = 13;

		} 
		#endregion //Level check

		switch (craftLevel) {
		case 1:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/potTanamanBtn").SetActive (true);
			break;
		case 2:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/celenganBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/celenganBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/celenganBtn/Text").GetComponent<Text> ().text = "Celengan";
			goto case 1;
		case 3:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tmpTisuBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tmpTisuBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tmpTisuBtn/Text").GetComponent<Text> ().text = "Tempat Tisu";
			goto case 2;
		case 4:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tasBelanjaBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tasBelanjaBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tasBelanjaBtn/Text").GetComponent<Text> ().text = "Tas Belanja";
			goto case 3;
		case 5:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tmpPensilBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tmpPensilBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tmpPensilBtn/Text").GetComponent<Text> ().text = "Tempat Pensil";
			goto case 4;
		case 7:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/mobilMiniaturBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/mobilMiniaturBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/mobilMiniaturBtn/Text").GetComponent<Text> ().text = "Mobil Miniatur";

			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/kapalMiniaturBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/kapalMiniaturBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/kapalMiniaturBtn/Text").GetComponent<Text> ().text = "Kapal Miniatur";
			goto case 5;
		case 9:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/celemekBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/celemekBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/celemekBtn/Text").GetComponent<Text> ().text = "Celemek";
			goto case 7;
		case 11:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/bungaHiasBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/bungaHiasBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/bungaHiasBtn/Text").GetComponent<Text> ().text = "Bunga Hias";
			goto case 9;
		case 12:
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tikarBtn").SetActive (true);
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tikarBtn").GetComponent<Button> ().interactable = true;;
			GameObject.Find ("Canvas/Craft Panel/Grid and Detail Panel/Grid Kerajinan/tikarBtn/Text").GetComponent<Text> ().text = "Tikar";
			goto case 11;
		} //level check untuk kerajinan

		switch (crewLevel){
		case 3:
			crewEffect = 0.3f;
			break;
		case 5:
			crewEffect = 0.5f;
			break;
		case 9:
			crewEffect = 0.7f;
			break;
		case 10:
			crewEffect = 0.8f;
			break;
		case 13:
			crewEffect = 0.9f;
			Debug.Log (crewEffect);
			break;
		} //level check untuk crew effect

		craftTitle.text = craftingName;
		craftImage.sprite = Resources.Load<Sprite>("Images/Kerajinan/" + craftingName) as Sprite;
		fabricText.text = System.Convert.ToString (fabricReq) + " Kain";
		paperText.text = System.Convert.ToString (paperReq) + " Kertas";
		plasticText.text = System.Convert.ToString (plasticReq) + " Plastik";
		earnMoneyExp.text = "Uang Kas : Rp. " + earnMoneyReq.ToString ("N2") + " dan Poin : " + earnExpReq.ToString ();
		durationText.text = durationReq.ToString () + " detik";
	}
}
