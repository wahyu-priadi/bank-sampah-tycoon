﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour {

    public Transform[] spawnLocation;
    public GameObject[] spawnPrefab;
    public GameObject[] spawnClone;
    public Quaternion rotation = Quaternion.identity;
    public int x = 0;

    public void OnTrigger()
    {
        float getRotationX = spawnLocation[x].transform.eulerAngles.x;
        float getRotationY = spawnLocation[x].transform.eulerAngles.y;
        float getRotationZ = spawnLocation[x].transform.eulerAngles.z;
        spawnClone[0] = Instantiate(spawnPrefab[0], spawnLocation[x].transform.position, Quaternion.Euler(getRotationX, getRotationY, getRotationZ)) as GameObject;
        x++;
    }


}