﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class profile : MonoBehaviour {

	//Script Interaction
	public economics Economics;
	public information Information;
	public rank Rank;

	public int year, month, week, time;
	public static int monthRank;
	public static float levelup, level, exp;
	public Text dates, exps, currentLevel;
	public bool timeContinue;
	public int[] levelToUnlockCraft, levelToUnlockCampaign, levelToAddCrew;


	void Start () {

		levelToUnlockCraft = new int[] { 2, 3, 4, 5, 7, 9, 11, 12};
		//levelToUnlockCampaign = new int[] { 8, 12, 15, 18};
		levelToAddCrew = new int[] { 3, 5, 7, 9, 11};

		year = 0; month = 0; week = 0; level = 1;
		timeContinue = true;
		Time.timeScale = 1;
		exp = 0;
		levelup = 500;


	}

	void Update () {
		if (timeContinue) {
			time++;
			while (time == 480) {
				time = 0;
				week++;
				while (week == 5) {
					Economics.SubMoneyPerMonth ();
					week = 0;
					month++;
					while (month == 12) {
						month = 0;
						year++;
					}
				}
			}
		};
			
		if (exp >= levelup) {
			if (level <= 30) {
				level++;
				levelup = levelup * 1.5f;
			} else if (level > 30) {
				level = 30;
				levelup = 0;
			}

			foreach (var x in levelToUnlockCraft){
				if (level == x){
					Information.CraftItemUnlocked();
				}
			}
//			foreach (var y in levelToUnlockCampaign){
//				if (level == y){
//					Information.CampaignItemUnlocked();
//				}
//			}
			foreach (var z in levelToAddCrew){
				if (level == z){
					Information.NewCrewUnlocked();
				}
			}
		}
			

		if (month == 6 && month == 0 && week == 1 && time == 0) {
			Rank.doRank ();
		} else if (year >= 1 && month == 0 && week == 1 && time == 0) {
			Rank.doRank ();
		}

		dates.text = System.Convert.ToString ("Umur : " + year + " Tahun - " + month + " Bulan - " + week + " Minggu");
		exps.text = "Skor : " + exp.ToString("F0") + " poin\nButuh " + (levelup - exp).ToString("F0") + " poin untuk naik level ";
		currentLevel.text = "Level\n" + level;
	}

}
