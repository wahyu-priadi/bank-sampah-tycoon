﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class hoverText : MonoBehaviour {
	public Text descText;
	public GameObject theText;
	private GameObject popText = null;
	private Transform mousePos, canvas;
	private RectTransform rt;
	private float widthPopText;

	public GameObject[] buttonDesc; 

	public void PointerEnter(GameObject ObjectHover){
		switch (ObjectHover.name) {
		case ("craft-button"):
			buttonDesc [0].SetActive (true);
			break;
		case ("campaign-button"):
			buttonDesc [1].SetActive (true);
			break;
		case ("menu-button"):
			buttonDesc [2].SetActive (true);
			break;
		case ("Resources Panel"):
			buttonDesc [3].SetActive (true);
			break;
//		case ("Pop"):
//			if (!popText) {
//				popText = Instantiate (theText, new Vector3 (Input.mousePosition.x, Input.mousePosition.y), Quaternion.Euler (0, 0, 0), GameObject.Find ("Canvas").transform) as GameObject;
//			}
//			break;
		}
	}

	public void PointerExit (){
		foreach (var desc in buttonDesc) {
			desc.SetActive (false);
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


		
		if (popText != null) {
			popText.transform.position = new Vector3 (Input.mousePosition.x - 50, Input.mousePosition.y + 20);
		}
	}
}
