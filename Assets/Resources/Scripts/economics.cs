﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class economics : MonoBehaviour {

	//Script Interaction
	public GameObject getCraftProgress;
	public craftProgress CraftProgress;
	public profile Profile;
	public information Information;
	public campaign Campaign;


	public static bool buletinActive, televisiActive = false;
	public float money;
	public int fabricRes, paperRes, plasticRes;
	public int fabric, paper, plastic;
	public static int peoplePerMonth;
	public string moneyIDR;
	public Text fabricText, paperText, plasticText, moneyText, expenseText;

	public int minRange, maxRange, c;

	void Start (){

		minRange = 1;
		maxRange = 5;

		money = 50000f;
		peoplePerMonth = 0;

		fabricRes = 50;
		paperRes = 50;
		plasticRes = 50;

		if (buletinActive) {
			getCampaignType ("Buletin");
		} else if (televisiActive) {
			getCampaignType ("Televisi");
		}

	}

	void OnTriggerEnter (Collider Other){
		//peoplePerMonth++;
		switch (c) {
		case 0:
			peoplePerMonth = peoplePerMonth + fabric;
			fabricRes = fabricRes + fabric;
			break;
		case 1:
			peoplePerMonth = peoplePerMonth + paper;
			paperRes = paperRes + paper;
			break;
		case 2:
			peoplePerMonth = peoplePerMonth + plastic;
			plasticRes = plasticRes + plastic;
			break;
		}
	} //Tambah resource
		
	public void AddMoney(){
		money = money + CraftProgress.earnMoney;
		profile.exp = profile.exp + CraftProgress.earnExp;

		getCraftProgress.SetActive (false);
	}
		
	public void SubMoneyPerMonth(){
		money = money - (350f * peoplePerMonth);
		peoplePerMonth = 0;
	}

	public void SubMoneyForCampaign (string media){
		switch (media) {
		case ("Pamflet"):
			money = money - 100000f;
			break;
		case ("Buletin"):
			money = money - 200000f;
			break;
		case ("Majalah"):
			money = money - 300000f;
			break;
		case ("Televisi"):
			money = money - 400000f;
			break;
		}
	}

	public void getCampaignType(string type){
		switch (type) {
		case ("Buletin"):
			minRange = 3;
			maxRange = 7;
			break;
		case ("Televisi"):
			minRange = 5;
			maxRange = 10;
			break;
		}
	}

	void Update (){
		c = Random.Range (0, 3);
		fabric = Random.Range (minRange, maxRange);
		paper = Random.Range (minRange, maxRange);
		plastic = Random.Range (minRange, maxRange);

		moneyText.text = "Rp. " + money.ToString ("N2");
		expenseText.text = "Rp. " + (350f * peoplePerMonth).ToString("N2");
		fabricText.text = fabricRes.ToString ();
		paperText.text = paperRes.ToString ();
		plasticText.text = plasticRes.ToString ();
	}
}
