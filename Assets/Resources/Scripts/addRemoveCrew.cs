﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class addRemoveCrew : MonoBehaviour {

//	public GameObject getMeja;
//	private actionButton meja;

	public Image addRemoveCrewIcon;
	public int no_meja;
	public GameObject crew_meja, meja_craft;
	private GameObject assigned_crew, assigned_meja = null;
	private bool Assigned = false;
	public bool defaultLoc;
	public Transform posisi;
	public GameObject targetLoc;


	// Use this for initialization
	void Start () {
//		meja = getMeja.GetComponent<actionButton> ();
		crew_meja = Resources.Load<GameObject>("Prefabs/crew_meja") as GameObject;
		meja_craft = Resources.Load<GameObject>("Prefabs/meja_craft") as GameObject;

		if (defaultLoc) {
			assigned_crew = (GameObject) Instantiate (crew_meja, posisi.transform.position, posisi.transform.rotation);
			Destroy (assigned_meja);
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (!Assigned) {
			addRemoveCrewIcon.sprite = Resources.Load<Sprite> ("Buttons/crew-add-button")as Sprite;
			targetLoc.SetActive (false);
		} else {
			addRemoveCrewIcon.sprite = Resources.Load<Sprite> ("Buttons/crew-remove-button")as Sprite;
			targetLoc.SetActive (true);
		}

	}
		
	public void changeIcon (){
		if (!Assigned) {
			addRemoveCrewIcon.sprite = null;
			addRemoveCrewIcon.sprite = Resources.Load<Sprite> ("Buttons/crew-add-button")as Sprite;
			Destroy (assigned_meja);
//			Destroy (meja.default_meja[no_meja]);z
			assigned_crew = (GameObject) Instantiate (crew_meja, posisi.transform.position, posisi.transform.rotation);
			targetLoc.SetActive (false);
			Assigned = true;
		} else {
			addRemoveCrewIcon.sprite = null;
			addRemoveCrewIcon.sprite = Resources.Load<Sprite> ("Buttons/crew-remove-button")as Sprite;
			Destroy (assigned_crew);
			assigned_meja = (GameObject) Instantiate (meja_craft, posisi.transform.position, posisi.transform.rotation);
			targetLoc.SetActive (true);
			Assigned = false;
		}
	}
}
