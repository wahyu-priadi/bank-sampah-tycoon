﻿using UnityEngine;
using System.Collections;

public class backgroundMusic : MonoBehaviour {

	public static backgroundMusic instance = null;
	public AudioSource bgMusic;
	public AudioClip bgMusicClip;
	public int queueAudio, prevQueue;

	// Use this for initialization
	void Start () {
		prevQueue = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (!bgMusic.isPlaying) {
			queueAudio = Random.Range (1, 3);
			if (queueAudio != prevQueue) {
				prevQueue = queueAudio;
				bgMusicClip = Resources.Load ("Audio/bgm" + queueAudio) as AudioClip;
				bgMusic.clip = bgMusicClip;
				bgMusic.Play ();
			} 
		}

	}
}
