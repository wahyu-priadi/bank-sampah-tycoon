﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class menu : MonoBehaviour {

	public GameObject settingPanel, aboutPanel, dimPanel, savedFound, loading;
	public Slider musicSlider, sfxSlider;
	public float musicVolume, sfxVolume, tempMusic, tempSfx;
	public static bool loadIsTrue;

	void Start(){
		loadIsTrue = false;
	}

	public void NewGame(){
		SceneManager.LoadSceneAsync ("scene1", LoadSceneMode.Single);

	}

	public void ResumeGame(){
		if (File.Exists (Application.persistentDataPath + "/player.sav")) {
			loading.SetActive (true);
			SceneManager.LoadSceneAsync ("scene1", LoadSceneMode.Single);
			loadIsTrue = true;
		} else if (!File.Exists (Application.persistentDataPath + "/player.sav")) {
			savedFound.SetActive (true);
			Invoke ("closeNotFound", 2f);
		}

	}

	public void closeNotFound(){
		savedFound.SetActive (false);
	}
		
	public void ExitGame(){
		Application.Quit ();
	}

	public void About(){
		aboutPanel.SetActive (true);
		dimPanel.SetActive (true);
	}

	public void BackAbout(){
		aboutPanel.SetActive (false);
		dimPanel.SetActive (false);
	}



}
