﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public economics Economics;
	public profile Profile;
	public craft Craft;
	public campaign Campaign;
	public rank Rank;
	public actionButton ActionButton;
	public information Information;
	public backgroundMusic BackgroundMusic;
	public soundEffect SoundEffect;
	public tutorial Tutorial;

	//public float fabricRes, paperRes, plasticRes, money, exp, level, levelup, minRange, maxRange, peoplePerMonth;

	public bool pamfletOn, majalahOn, buletinOn, televisiOn, bgmON, sfxON;
	public int fabricRes, paperRes, plasticRes, minRange, maxRange, peoplePerMonth;
	public int time, week, month, year;
	public float money, exp, level, levelup, currentRank;

	void Update() {
		if (menu.loadIsTrue) {
			Load ();
			Tutorial.gameObject.SetActive (false);
			Time.timeScale = 1;
			Profile.timeContinue = true;
			menu.loadIsTrue = false;
		} 
	}

	public void Save(){
		#region boolStats
		pamfletOn = craft.pamfletActive;
		buletinOn = economics.buletinActive;
		majalahOn = craft.majalahActive;
		televisiOn = economics.televisiActive;

		bgmON = ActionButton.bgmON;
		sfxON = ActionButton.sfxON;
		#endregion
		#region intStats
		fabricRes = Economics.fabricRes;
		paperRes = Economics.paperRes;
		plasticRes = Economics.plasticRes;
		minRange = Economics.minRange;
		maxRange = Economics.maxRange;
		peoplePerMonth = economics.peoplePerMonth;
	
		time = Profile.time;
		week = Profile.week;
		month = Profile.month;
		year = Profile.year;

		#endregion
		#region floatStats
		money = Economics.money;
		exp = profile.exp;
		level = profile.level;
		levelup = profile.levelup;
		currentRank = Rank.currentRank;
		#endregion
		SaveLoadManager.SavePlayer (this);

		Debug.Log("Save Process");

		Information.GameSaved ();
		ActionButton.confirmSave.SetActive (false);
		ActionButton.settingPanel.SetActive (false);
		ActionButton.dimPanel.SetActive (false);
		Time.timeScale = 1;
		ActionButton.action_button.SetActive (true);
		Profile.timeContinue = true;
	}
		
	public void Load(){
		PlayerData loadedstats = SaveLoadManager.LoadPlayer ();

		craft.pamfletActive = loadedstats.boolStats [0];
		economics.buletinActive = loadedstats.boolStats [1];
		craft.majalahActive = loadedstats.boolStats [2];
		economics.televisiActive = loadedstats.boolStats [3];

		ActionButton.bgmToggle (loadedstats.boolStats [4]);
		ActionButton.sfxToggle (loadedstats.boolStats [5]);

		Economics.fabricRes = loadedstats.intStats [0];
		Economics.paperRes = loadedstats.intStats [1];
		Economics.plasticRes = loadedstats.intStats [2];
		Economics.minRange = loadedstats.intStats [3];
		Economics.maxRange = loadedstats.intStats [4];
		economics.peoplePerMonth = loadedstats.intStats [5];

		Profile.time = loadedstats.intStats [6];
		Profile.week = loadedstats.intStats [7];
		Profile.month = loadedstats.intStats [8];
		Profile.year = loadedstats.intStats [9];

		Economics.money = loadedstats.floatStats [0];
		profile.exp = loadedstats.floatStats [1];
		profile.level = loadedstats.floatStats [2];
		profile.levelup = loadedstats.floatStats [3];
		Rank.currentRank = loadedstats.floatStats [4];


	}

//	public void LoadPlayerPref(){
//		
//		Economics.fabricRes = PlayerPrefs.GetInt("fabricRes");
//		Economics.paperRes = PlayerPrefs.GetInt("paperRes");
//		Economics.plasticRes = PlayerPrefs.GetInt ("plasticRes");
//
//		Economics.money = PlayerPrefs.GetFloat("money");
//		Profile.exp = PlayerPrefs.GetFloat("exp");
//		Profile.level = PlayerPrefs.GetFloat("level");
//		Profile.levelup = PlayerPrefs.GetFloat("levelup");
//	}
}
