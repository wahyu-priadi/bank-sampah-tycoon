﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class soundEffect : MonoBehaviour {

	public AudioSource regClick;
	public AudioSource[] audioEvent;
	public AudioClip regClickClip, craftClickClip, incomeMoneyClip, craftingClip, cancelCraftingClip;

	void Start(){
		regClickClip = Resources.Load ("Audio/Reg_Click") as AudioClip;
		craftClickClip = Resources.Load ("Audio/Craft_Click") as AudioClip;
		incomeMoneyClip = Resources.Load ("Audio/Income_Money") as AudioClip;
		craftClickClip = Resources.Load ("Audio/Craft_Click") as AudioClip;
		craftingClip = Resources.Load ("Audio/Craft_Progress") as AudioClip;
		cancelCraftingClip = Resources.Load ("Audio/Cancel_Crafting") as AudioClip;
	}

	public void OnRegClick(){
		regClick.clip = regClickClip;
		regClick.Play ();

	}

	public void SFX(AudioClip clip){
		audioEvent [0].clip = clip;
		audioEvent [0].Play ();
	}

	public void CraftingSFX(bool isCrafting){
		audioEvent [1].clip = craftingClip;
		if (isCrafting)
			audioEvent [1].Play ();
		else
			audioEvent [1].Stop ();
	}
}
