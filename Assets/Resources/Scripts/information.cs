﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class information : MonoBehaviour {

	public economics Economics;
	public profile Profile;
	public actionButton ActionButton;
	public rank Rank;

	public GameObject infoPanel, unlockedCraftItem, unlockedCampaignItem, addedNewCrew, gameSaved, winPanel, bankruptPanel, rankPanel;
	public Text rankText, winText;
	public Button OKButton;
	public bool OKisClicked = false;

	void Update(){
		if (Economics.money < 0f) {
			infoPanel.SetActive (true);
			bankruptPanel.SetActive (true);
			Time.timeScale = 0;
			Profile.timeContinue = false;
		} else if (Profile.year == 1 && Profile.month == 1 && Profile.week == 0 && Profile.time == 0) {
			infoPanel.SetActive (true);
			winPanel.SetActive (true);
			winText.text = "Selamat kamu telah memenangkan permainan dengan sukses mencapai 1 tahun.\n" +
			"Kamu tetap bisa melanjutkan permainan.\n" +
				"Peringkat terakhir yang kamu dapat : " + Rank.currentRank.ToString();
			Time.timeScale = 0;
			Profile.timeContinue = false;
		}
	}

	public void CraftItemUnlocked(){
		unlockedCraftItem.SetActive (true);
		Invoke ("closeAllInfo", 5f);
	}

	public void CampaignItemUnlocked(){
		unlockedCampaignItem.SetActive (true);
		Invoke ("closeAllInfo", 5f);
	}

	public void NewCrewUnlocked(){
		addedNewCrew.SetActive (true);
		Invoke ("closeAllInfo", 5f);
	}

	public void GameSaved(){
		gameSaved.SetActive (true);
		Invoke ("closeAllInfo", 2f);
	}

	public void closeAllInfo(){
		unlockedCraftItem.SetActive (false);
		unlockedCampaignItem.SetActive (false);
		addedNewCrew.SetActive (false);
		gameSaved.SetActive (false);
	}

	public void Ranked (float rank){
		infoPanel.SetActive (true);
		rankPanel.SetActive (true);
		rankText.text = "Perlombaan Bank Sampah di kota sudah dimulai dan hasil lomba sudah keluar." +
		"Pada periode ini, bank sampah kamu menduduki peringkat...\n" + rank.ToString ();
	}

	public void Bankrupt (){
		infoPanel.SetActive (true);
		bankruptPanel.SetActive (true);
	}

	public void ClickOK(){
		infoPanel.SetActive (false);
		rankPanel.SetActive (false);
		winPanel.SetActive (false);
		bankruptPanel.SetActive (false);
		Time.timeScale = 1;
		Profile.timeContinue = true;
	}

	public void BackToMainMenu(){
		infoPanel.SetActive (false);
		bankruptPanel.SetActive (false);
		ActionButton.ConfirmExit ();
	}

}
