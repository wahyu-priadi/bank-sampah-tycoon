﻿using UnityEngine;
using System.Collections;

public class animationController : MonoBehaviour {

	public actionButton ActionButton;

	public GameObject teller;
	public GameObject[] crew, smokes;
	public Animation tellerAnim, crewAnim;

	void Start () {
		tellerAnim = teller.GetComponent<Animation> ();
		tellerAnim ["playTeller"].speed = 0.3f;
		tellerAnim.Play ();

	}

	void Update(){
		crew = GameObject.FindGameObjectsWithTag ("Crew");

	}

	public void crewAnimation(int crewAni){
		switch (crewAni) {
		case 1:
			foreach (GameObject animate in crew) {
				crewAnim = animate.GetComponent<Animation> ();
				crewAnim.Play ("playCrew");
			}

			switch (ActionButton.checkLevel) {
			case 1:
				smokes [4].SetActive (true);
				break;
			case 3:
				smokes [2].SetActive (true);
				goto case 1;
			case 5:
				smokes [0].SetActive (true);
				goto case 3;
			case 7:
				smokes [5].SetActive (true);
				goto case 5;
			case 9:
				smokes [3].SetActive (true);
				goto case 7;
			case 11:
				smokes [1].SetActive (true);
				goto case 9;
			}

			break;

		case 2:
			foreach (GameObject animate in crew) {
				crewAnim = animate.GetComponent<Animation> ();
				crewAnim.Play ("stayCrew"); 
			}

			switch (ActionButton.checkLevel) {
			case 1:
				smokes [4].SetActive (false);
				break;
			case 3:
				smokes [2].SetActive (false);
				goto case 1;
			case 5:
				smokes [0].SetActive (false);
				goto case 3;
			case 7:
				smokes [5].SetActive (false);
				goto case 5;
			case 9:
				smokes [3].SetActive (false);
				goto case 7;
			case 11:
				smokes [1].SetActive (false);
				goto case 9;
			}
			break;
		}
	}
}
