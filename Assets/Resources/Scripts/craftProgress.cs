using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class craftProgress : MonoBehaviour {

	//Script Interaction
	public craft Craft;
	public economics Economics;
	public soundEffect SoundEffect;
	public animationController AnimationController;

	public GameObject progress, action_button;
	public Button crewPanelBtn, campaignPanelBtn;
	public Slider progressSlider;
	public Text craftingName, multi;

	public AudioClip IncomeMoney;

	public float earnMoney, p, q;
	public float earnExp;
	public bool finishCraft = false;
	public bool doProgressRun;
	private Coroutine co;

	void Start () {
		progressSlider = progress.GetComponent<Slider> ();

	}

	void Update () {
		craftingName.text = "Membuat " + Craft.craftingName;
		earnMoney = Craft.earnMoneyReq;
		earnExp = Craft.earnExpReq;
		progressSlider.maxValue = Craft.durationReq;

		if (doProgressRun) {
			co = StartCoroutine (doProgress ());
		}

	}

	public IEnumerator doProgress(){
		doProgressRun = false;
		SoundEffect.CraftingSFX (true);
		AnimationController.crewAnimation (1);
	
		for (q = 0; q < craft.multi; q++) {
			multi.text = (q + 1) + " dari " + craft.multi; 
			float animationTime = 0f;
			while (animationTime < Craft.durationReq) {
				animationTime += Time.deltaTime;
				float lerpTime = animationTime / Craft.durationReq;
				progressSlider.value = Mathf.Lerp (0, Craft.durationReq, lerpTime);
				yield return null;
			}
			progressSlider.value = 0;
		}

		AnimationController.crewAnimation (2);
		progressSlider.value = 0;
		Economics.AddMoney();
		crewPanelBtn.interactable = true;
		campaignPanelBtn.interactable = true;
		action_button.SetActive (true);
		SoundEffect.SFX(SoundEffect.incomeMoneyClip);
		SoundEffect.CraftingSFX (false);
	}

	public void CancelCraft(){
		StopCoroutine (co);
		AnimationController.crewAnimation (2);
		progressSlider.value = 0;
		crewPanelBtn.interactable = true;
		campaignPanelBtn.interactable = true;
		SoundEffect.CraftingSFX (false);
		SoundEffect.SFX(SoundEffect.cancelCraftingClip);
		gameObject.SetActive (false);
		action_button.SetActive (true);
	}
}
