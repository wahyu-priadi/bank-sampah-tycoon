﻿using UnityEngine;
using System.Collections;

public class spawnPeople : MonoBehaviour {

	public GameObject[] people;
	public Vector3 spawnValues;
	public float spawnWait;
	public float spawnMostWait;
	public float spawnLeastWait;
	public int startWait;
	public bool stop;

	int randPeople;
	int urutan = 0;


	// Use this for initialization
	void Start () {
		StartCoroutine (waitSpawner ());
	}
	
	// Update is called once per frame
	void Update () {
		spawnWait = Random.Range (spawnLeastWait, spawnMostWait);
	}

	IEnumerator waitSpawner ()
	{
		yield return new WaitForSeconds (startWait);
		while (!stop) {
			randPeople = Random.Range (0, 3);
			Instantiate (people[randPeople], gameObject.transform.position, gameObject.transform.rotation);
			urutan++;
			yield return new WaitForSeconds (spawnWait);
		}
	}
		
}
