﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoadManager {

	public Player setPlayer;

	public static void SavePlayer(Player player){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream stream = new FileStream(Application.persistentDataPath + "/player.sav", FileMode.Create);

		PlayerData data = new PlayerData (player);

		bf.Serialize (stream, data);
		stream.Close ();
		Debug.Log("Saved to " + Application.persistentDataPath);
	}

	public static PlayerData LoadPlayer(){
		if (File.Exists (Application.persistentDataPath + "/player.sav")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream stream = new FileStream (Application.persistentDataPath + "/player.sav", FileMode.Open);

			PlayerData data = bf.Deserialize (stream) as PlayerData;
			stream.Close ();
			return data;
		} else {
			Debug.LogError ("File does not exist");
			return null;
		}
	}
}

[Serializable]
public class PlayerData{
	public bool[] boolStats;
	public int[] intStats;
	public float[] floatStats;

	public PlayerData(Player player){
		boolStats = new bool[6];
		boolStats [0] = player.pamfletOn;
		boolStats [1] = player.buletinOn;
		boolStats [2] = player.majalahOn;
		boolStats [3] = player.televisiOn;
		boolStats [4] = player.bgmON;
		boolStats [5] = player.sfxON;

		intStats = new int[10];
		intStats [0] = player.fabricRes;
		intStats [1] = player.paperRes;
		intStats [2] = player.plasticRes;
		intStats [3] = player.minRange;
		intStats [4] = player.maxRange;
		intStats [5] = player.peoplePerMonth;
		intStats [6] = player.time;
		intStats [7] = player.week;
		intStats [8] = player.month;
		intStats [9] = player.year;


		floatStats = new float[5];
		floatStats[0] = player.money;
		floatStats[1] = player.exp;
		floatStats[2] = player.level;
		floatStats[3] = player.levelup;
		floatStats[4] = player.currentRank;

//		stats = new float[10];
//		stats [0] = player.fabricRes;
//		stats [1] = player.paperRes;
//		stats [2] = player.plasticRes;
//		stats [3] = player.money;
//		stats [4] = player.exp;
//		stats [5] = player.level;
//		stats [6] = player.levelup;
//		stats [7] = player.minRange;
//		stats [8] = player.maxRange;
//		stats [9] = player.peoplePerMonth;
	
	}
}
